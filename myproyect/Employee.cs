﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myproyect
{
    class Employee
    {
        private int IdEmployee;

        public void setIdEmployee(int IdEmployee)
        {
            this.IdEmployee = IdEmployee;
        }

        public int getIdEmployee()
        {
            return this.IdEmployee;
        }

        private string Name;

        public void setName(string Name)
        {
            this.Name = Name;
        }

        public string getName()
        {
            return this.Name;
        }

        private string lastName;

        public void setLastName(String lastName)
        {
            this.lastName = lastName;
        }

        public string getLastName()
        {
            return this.lastName;
        }

    }
}
